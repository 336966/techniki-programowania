#ifndef INSTRUKCJA_H_INCLUDED
#define INSTRUKCJA_H_INCLUDED
#include <iostream>
#include <map>

using namespace std;

template <typename S, typename D>
class szablon
    {
    public :

        void petla();
        void dodaj();
        void usun();
        void wyswietl();

    private :
        map<S, D> _indeks;
};


#endif // INSTRUKCJA_H_INCLUDED
