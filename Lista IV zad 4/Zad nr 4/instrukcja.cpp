#include "instrukcja.h"
#include <iostream>

template <typename S, typename D>
void szablon<S, D>::dodaj(){
    S a;
    D b;

    cout << endl << "Podaj nazwe kursu: ";
    cin >> a;
    cout << "Podaj ocene: ";
    cin >> b;
    cout << endl;

    _indeks[a] = b;
}

template <typename S, typename D>
void szablon<S, D>::usun(){
    S a;

    cout << endl << "Podaj nazwe kursu: ";
    cin >> a;
    cout << endl;

    _indeks.erase(a);
}

template <typename S, typename D>
void szablon<S, D>::wyswietl(){
    cout << endl << "Lista ocen:" << endl;
    for(typename map<S, D>::iterator itr = _indeks.begin(),
        koniec = _indeks.end(); itr != koniec; itr++){
            cout << itr->first << '\t' << " --> " << '\t' << itr->second << endl;
    }
    cout << endl;
}

template <typename S, typename D>
void szablon<S, D>::petla(){

    bool x = true;

     while(x)
        {
    system("cls");
    cout << "Indeks 255906" << endl << endl;
    cout << "Co chcesz zrobic:" << endl;
    cout << "1 Wprowadz nowa ocene za kurs/zmien ocene za kurs" << endl;
    cout << "2 Usun ocene za kurs" << endl;
    cout << "3 Wyswietl oceny" << endl;
    cout << ">>";

    int a;
    cin >> a;

    if (a<1 || a>3) continue;
    switch(a)
        {
        case 1 : dodaj();
        break ;
        case 2 : usun();
        break ;
        case 3 : wyswietl();
        break ;
    }

    int b;
    cout << "1 Baw sie dalej" << endl;
    cout << "2 Zamknij program" << endl << ">>";
    cin >> b;

        switch(b)
        {
        case 1 : break ;
        case 2 : x = false;
        break;
    }
    }
}
