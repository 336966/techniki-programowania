#include <iostream>
#include "complex0.h"

using std::cout;
using std::endl;
using std::cin;

l_zespolone::l_zespolone()
            :   _a(0)
            ,   _b(0)
{}

void l_zespolone::dod()
            {
            float a,b,c,d;
            cout << "Podaj pierwsza liczbe zespolona/rzeczywista z=a+bi" << endl;
            cout << "Przy wyborze liczby R w polu 'b:' prosze wpisac '0'" << endl;
            cout << "Podaj a:";
            cin >> a;
            cout << "Podaj b:";
            cin >> b;
            cout << "Podaj druga liczbe zespolona/rzeczywista z=a+bi" << endl;
            cout << "Przy wyborze liczby R w polu 'b:' prosze wpisac '0'" << endl;
            cout << "Podaj a:";
            cin >> c;
            cout << "Podaj b:";
            cin >> d;

               _a = (a+c);
               _b = (b+d);
               wypisz();

}

void l_zespolone::ode()
            {
            float a,b,c,d;
            cout << "Podaj pierwsza liczbe zespolona/rzeczywista z=a+bi" << endl;
            cout << "Przy wyborze liczby R w polu 'b:' prosze wpisac '0'" << endl;
            cout << "Podaj a:";
            cin >> a;
            cout << "Podaj b:";
            cin >> b;
            cout << "Podaj druga liczbe zespolona/rzeczywista z=a+bi" << endl;
            cout << "Przy wyborze liczby R w polu 'b:' prosze wpisac '0'" << endl;
            cout << "Podaj a:";
            cin >> c;
            cout << "Podaj b:";
            cin >> d;

               _a = (a-c);
               _b = (b-d);
                wypisz();
}

void l_zespolone::mno()
            {
            float a,b,c,d;
            cout << "Podaj pierwsza liczbe zespolona/rzeczywista z=a+bi" << endl;
            cout << "Przy wyborze liczby R w polu 'b:' prosze wpisac '0'" << endl;
            cout << "Podaj a:";
            cin >> a;
            cout << "Podaj b:";
            cin >> b;
            cout << "Podaj druga liczbe zespolona/rzeczywista z=a+bi" << endl;
            cout << "Przy wyborze liczby R w polu 'b:' prosze wpisac '0'" << endl;
            cout << "Podaj a:";
            cin >> c;
            cout << "Podaj b:";
            cin >> d;

               _a = (a*c-b*d);
               _b = (a*d+b*c);
                wypisz();
}

void l_zespolone::spr()
            {
            float a,b;
            cout << "Podaj liczbe zespolona z=a+bi" << endl;
            cout << "Podaj a:";
            cin >> a;
            cout << "Podaj b:";
            cin >> b;

            _a = a;
            _b = (-b);
            wypisz();
}

void l_zespolone::zer()
            {
                _a = 0;
                _b = 0;
}

void l_zespolone::wypisz()
            {
            std::cout << "Wynik: z=" << _a;
            if (_b>=0)   std::cout << "+";
            std::cout << _b << "i" << std::endl;


    }
