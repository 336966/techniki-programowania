#include <iostream>


using namespace std;

int main()
{
    int rozmiar;
    cout << "Podaj rozmiar tabliczki: " ;
    cin >> rozmiar;

    // Gdy uzytkownik poda liczbe mniejsza od 0
    if(rozmiar < 0) rozmiar = rozmiar * -1;



    // Alokacja talicy dwuwymiarowej
    int **tabliczka = new int *[rozmiar];
    for (int i=0; i<rozmiar; i++)
        tabliczka[i] = new int [rozmiar];

    // Mnozenie
    for (int i=0; i<rozmiar; i++)
        {
        for (int j=0; j<rozmiar; j++)
            {
            tabliczka [i][j] = (i+1)*(j+1);
        }
    }

    // Wypisanie
    for (int i=0; i<rozmiar; i++)
        {
        for (int j=0; j<rozmiar; j++)
            {
            cout << tabliczka [i][j] << "\t";
        }
        cout << endl;
    }

    // Usuwanie tablicy
    for (int i=0; i<rozmiar; i++)
        delete[] tabliczka[i];
    delete[] tabliczka;

    return 0;
}
