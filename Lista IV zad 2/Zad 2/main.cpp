#include <iostream>
#include "Figure.h"
#include "Pochodne.h"

using namespace std;

int main()
{
    double r,a,b,x,y,z;
    float t;

    Figure F;
    Circle C;
    Triangle T;
    Rectangle R;

    cout << "Zadanie 2: klasa bazowa oraz klasy pochodne" << endl << endl;
    cout << "Podaj promien kola: ";
    cin >> r;
    cout << endl << "Podaj dlugosc bokow trojkata" << endl << "1: ";
    cin >> x;
    cout << "2: ";
    cin >> y;
    cout << "3: ";
    cin >> z;
    cout << endl << "Podaj dlugosc bokow prostokata" << endl << "1: ";
    cin >> a;
    cout << "2: ";
    cin >> b;
    cout << endl;

    C.Set_R(r);
    T.Set_X_Y_Z(x, y, z);
    R.Set_A_B(a, b);

    cout << "Pole kola = " << C.area() << endl;
    cout << "pole trojkata = " << T.area() << endl;
    cout << "Pole prostokata = " << R.area() << endl;


    return 0;
}
