#include "Pochodne.h"
#include <iostream>
#include <math.h>

Circle::Circle()
        :   _R(0)
{}

double Circle::area ()
        {
        const double pi = 3.14159265359;
        return pi*_R*_R;
}

void Circle::Set_R (double r)
        {
        _R = r;
}

Triangle::Triangle()
        :   _X(0)
        ,   _Y(0)
        ,   _Z(0)
{}


double Triangle::area ()
        {
        double p = (_X+_Y+_Z)/2;
        return sqrt(p*(p-_X)*(p-_Y)*(p-_Z));
}

void Triangle::Set_X_Y_Z (double x, double y, double z)
        {
        _X = x;
        _Y = y;
        _Z = z;
}

Rectangle::Rectangle()
        :   _A(0)
        ,   _B(0)
{}

double Rectangle::area ()
        {
        return _A*_B;
}

void Rectangle::Set_A_B (double a, double b)
        {
        _A = a;
        _B = b;
}
