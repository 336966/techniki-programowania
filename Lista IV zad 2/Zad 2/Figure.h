#ifndef FIGURE_H_INCLUDED
#define FIGURE_H_INCLUDED



class Figure
    {
        public :
            Figure();
            //virtual ~Figure();
            virtual double area ();
            virtual void Set_a();

        private:
            int _a;
};



#endif // FIGURE_H_INCLUDED
