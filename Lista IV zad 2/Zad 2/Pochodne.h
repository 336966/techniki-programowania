#ifndef POCHODNE_H_INCLUDED
#define POCHODNE_H_INCLUDED
#include "Figure.h"

class Circle : public Figure
    {

    public:
        virtual double area ();
        Circle();
        void Set_R(double r);

    private:
        double _R;
};

class Triangle : public Figure
    {

    public:
        virtual double area ();
        Triangle();
        void Set_X_Y_Z(double x, double y, double z);

    private:
        double _X;
        double _Y;
        double _Z;
};

class Rectangle : public Figure
    {

    public:
        virtual double area ();
        Rectangle();
        void Set_A_B(double a, double b);

    private:
        double _A;
        double _B;
};

#endif // POCHODNE_H_INCLUDED




