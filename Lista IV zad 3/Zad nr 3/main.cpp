#include <iostream>
#include <map>
#include <fstream>

using namespace std;


int koniec();
void wyswietl();
void menu();
void usun();
void odczyt();
void zapisz();
int zmiana();
void dodaj();

map<string, string> ksiazka_adresowa;

int main()
{
    int a,b;
    bool x = true;

    odczyt();


    while(x)
        {
    system("cls");
    menu();
    cin >> a;

    if (a<1 || a>4) continue;
    switch(a)
        {
        case 1 : dodaj();
        break ;
        case 2 : usun();
        break ;
        case 3 : zmiana();
        break ;
        case 4 : wyswietl();
        break ;
    }

    b = koniec();
    switch(b)
        {
        case 1 : break ;
        case 2 : x = false;
        break;
    }
}

    zapisz();
    return 0;
}

int zmiana()
    {
    string a,b,c;
    cout << "Podaj imie uzytkownika: ";
    cin >> a;
    cout << "Podaj nazwisko uzytkownika: ";
    cin >> b;
    string d = a + " " + b;
    cout << "Podaj email ktory chcesz przypisac temu uzytkownikowi: ";
    cin >> c;
    int x,y;
    y = ksiazka_adresowa.size();
    ksiazka_adresowa [d] = c ;
    x = ksiazka_adresowa.size();
    if (x==y) {cout << "Zmieniono pomyslnie" << endl << endl; return 0;}
    if (x!=y) {ksiazka_adresowa.erase(d); cout << "Nie znaleziono uzytkownika" << endl << endl; return 0;}
}

void odczyt()
    {
    string imie,nazwisko,mail,d;
    ifstream read_file ("plik z adresami.txt");
    for(int i=0; 1; i++){
    if (!(read_file >> imie)) break;
    read_file >> nazwisko;
    read_file >> mail;
    d = imie + " " + nazwisko;
    ksiazka_adresowa[d] = mail;
    }
}

void zapisz()
    {
    ofstream write_file("plik z adresami.txt");
    for(map<string, string>::iterator itr = ksiazka_adresowa.begin(),
    koniec = ksiazka_adresowa.end(); itr != koniec; itr++){
        write_file << itr->first << "    " << itr->second << endl;
    }
}

void usun()
    {
    string a,b;
    int z,y;
    z = ksiazka_adresowa.size();    //wielkosc przed
    cout << "Podaj imie uzytkownika ktorego e-mail chcesz usunac: ";
    cin >> a;
    cout << "Podaj nazwisko uzytkownika ktorego e-mail chcesz usunac: ";
    cin >> b;
    string d = a + " " + b;
    ksiazka_adresowa.erase(d);
    y = ksiazka_adresowa.size();    //wielkosc po

    if(z!=y){cout << "Usunieto pomyslnie" << endl << endl;}
    else {cout << "Nie znaleziono takiego uzytkownika" << endl << endl;}
}

void menu()
    {
    cout << "Ksiazka adresowa" << endl << endl;
    cout << "Co chcesz zrobic:" << endl;
    cout << "1 Wprowadz nowego uzytkownika" << endl;
    cout << "2 Usun uzytkownika" << endl;
    cout << "3 Zmien email przypisany konkretnemu uzytkownikowi" << endl;
    cout << "4 Wyswietl zawartosc ksiazki adresowej" << endl;
    cout << ">>";
}

void dodaj()
{
    string a,b,c;
    cout << "Podaj imie: ";
    cin >> a;
    cout <<  "Podaj naziwsko: ";
    cin >> b;
    cout << "Podaj e-mail: ";
    cin >> c;
    cout << endl;
    string d = a + " " + b;
    ksiazka_adresowa[d] = c;
}

void wyswietl()
    {
    cout << endl << "Lista adresow:" << endl;
    for(map<string, string>::iterator itr = ksiazka_adresowa.begin(),
        koniec = ksiazka_adresowa.end(); itr != koniec; itr++){
            cout << itr->first << " --> " << itr->second << endl;
    }
    cout << endl;
}

int koniec()
    {
    int x;
    cout << "1 Baw sie dalej" << endl;
    cout << "2 Zamknij program" << endl << ">>";
    cin >> x;
    return x;
}
