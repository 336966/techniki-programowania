#include <iostream>
#include "TicTacToe.h"
#include <cstdlib>

using namespace std;

void TicTacToe::TworzPlansze()
{
    tab = new char *[3];
    for (int i=0; i<3; i++)
        tab[i] = new char [3];

         for (int i=0; i<3; i++)
        {
        for (int j=0; j<3; j++)
            {

            tab [i][j] = (char)Pola(puste);
        }
    }
}

void TicTacToe::Wypisz()
{
 cout << "Kolko i Krzyzyk" << endl << endl;

        //Wypisywanie planszy
       for (int i=0; i<3; i++)
        {
            cout << "  ";
            for (int j=0; j<3; j++)
            {
                cout << tab [i][j];
                if(j<2) cout << " | ";
            }
            if(i<2) cout << endl << " ----------- " << endl;
            else cout << endl << endl;
        }
}

void TicTacToe::Rozgrywka()
{
     cout << "Gracz " << ruch << " wybiera miejsce \n>> ";
        cin >> x >> y;
        while(x>3 || x<1 || y>3 || y<1 || tab[x-1][y-1] != (char)Pola(puste))
        {
            cout << "To miejsce jest zajete lub wpisujesz nie poprawna wartosc!\nWybierz inne: \n>>";
            cin >> x >> y;
        }
        tab[x-1][y-1] = ruch;
        if(ruch == (char)Pola(krzyzyk))
        {
            ruch = (char)Pola(kolko);
        }
        else
        {
           ruch = (char)Pola(krzyzyk);

        }
}

void TicTacToe::Sprawdz()
{

    if(
           tab[0][0] == (char)Pola(krzyzyk) && tab[0][1] == (char)Pola(krzyzyk) && tab[0][2] == (char)Pola(krzyzyk) ||
           tab[1][0] == (char)Pola(krzyzyk) && tab[1][1] == (char)Pola(krzyzyk) && tab[1][2] == (char)Pola(krzyzyk) ||
           tab[2][0] == (char)Pola(krzyzyk) && tab[2][1] == (char)Pola(krzyzyk) && tab[2][2] == (char)Pola(krzyzyk) ||
           tab[0][0] == (char)Pola(krzyzyk) && tab[1][0] == (char)Pola(krzyzyk) && tab[2][0] == (char)Pola(krzyzyk) ||
           tab[0][1] == (char)Pola(krzyzyk) && tab[1][1] == (char)Pola(krzyzyk) && tab[2][1] == (char)Pola(krzyzyk) ||
           tab[0][2] == (char)Pola(krzyzyk) && tab[1][2] == (char)Pola(krzyzyk) && tab[2][2] == (char)Pola(krzyzyk) ||
           tab[0][0] == (char)Pola(krzyzyk) && tab[1][1] == (char)Pola(krzyzyk) && tab[2][2] == (char)Pola(krzyzyk) ||
           tab[0][2] == (char)Pola(krzyzyk) && tab[1][1] == (char)Pola(krzyzyk) && tab[2][0] == (char)Pola(krzyzyk)
           ){
            isOver = true;
            Winer = (char)Pola(krzyzyk);
           }

        //Kółko
        if(
           tab[0][0] == (char)Pola(kolko) && tab[0][1] == (char)Pola(kolko) && tab[0][2] == (char)Pola(kolko) ||
           tab[1][0] == (char)Pola(kolko) && tab[1][1] == (char)Pola(kolko) && tab[1][2] == (char)Pola(kolko) ||
           tab[2][0] == (char)Pola(kolko) && tab[2][1] == (char)Pola(kolko) && tab[2][2] == (char)Pola(kolko) ||
           tab[0][0] == (char)Pola(kolko) && tab[1][0] == (char)Pola(kolko) && tab[2][0] == (char)Pola(kolko) ||
           tab[0][1] == (char)Pola(kolko) && tab[1][1] == (char)Pola(kolko) && tab[2][1] == (char)Pola(kolko) ||
           tab[0][2] == (char)Pola(kolko) && tab[1][2] == (char)Pola(kolko) && tab[2][2] == (char)Pola(kolko) ||
           tab[0][0] == (char)Pola(kolko) && tab[1][1] == (char)Pola(kolko) && tab[2][2] == (char)Pola(kolko) ||
           tab[0][2] == (char)Pola(kolko) && tab[1][1] == (char)Pola(kolko) && tab[2][0] == (char)Pola(kolko)
           ){
            isOver = true;
            Winer = (char)Pola(kolko);
           }
        if(isOver != true)
        remis = true;

            for(int i=0; i<3; i++)
                for(int j=0; j<3;j++)
                  if(tab[i][j]==' ')
                        remis = false;


        if(remis == true) isOver=true;
    //Czyszczenie ekranu
       system("cls");
}

void TicTacToe::Zakonczenie(){
    TicTacToe::Wypisz();

    if(remis==true)cout << "Remis!";
    else cout << "Wygrywa: " << Winer << endl;

    // Usuwanie tablicy
    for (int i=0; i<3; i++)
        delete[] tab[i];
    delete[] tab;



}


void TicTacToe::PetlaGry()
{
    while(!isOver){

        TicTacToe::Wypisz();
        TicTacToe::Rozgrywka();
        TicTacToe::Sprawdz();
    }
}

