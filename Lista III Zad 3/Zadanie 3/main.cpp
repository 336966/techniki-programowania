#include <iostream>
#include "TicTacToe.h"

using namespace std;

int main()
{
    TicTacToe * t = new TicTacToe();
    t->TworzPlansze();
    t->PetlaGry();
    t->Zakonczenie();
    delete t;
    return 0;
}
