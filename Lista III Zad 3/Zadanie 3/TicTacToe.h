#include <iostream>

using namespace std;

class TicTacToe
{
  private:
      enum Pola{krzyzyk = (int)'X',kolko=(int)'O', puste=(int)' '};
      char ruch = Pola(krzyzyk);
      string zwyciecza;
      bool isOver = false;
      bool remis = false;
      int x, y;
      char ** tab;
      char Winer;
      void Sprawdz();
      void Rozgrywka();
      void Wypisz();

    public:
      void PetlaGry();
      void TworzPlansze();
      void Zakonczenie();

};
