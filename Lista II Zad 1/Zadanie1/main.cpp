#include <iostream>
#include <cstdlib>

using namespace std;

int main()
{
    bool isOver = false;
    string Winer = "";
    char Gracz = 'X';
    string Player ="Krzyzk";
    int x, y;
    bool remis;
    int rozmiar = 3;

    // Alokacja talicy dwuwymiarowej
    char **tabliczka = new char *[rozmiar];
    for (int i=0; i<rozmiar; i++)
        tabliczka[i] = new char [rozmiar];


    for (int i=0; i<rozmiar; i++)
        {
        for (int j=0; j<rozmiar; j++)
            {
            tabliczka [i][j] = ' ';
        }
    }

    while(!isOver)
    {
        cout << "Kolko i Krzyzyk" << endl << endl;

        //Wypisywanie planszy
       for (int i=0; i<rozmiar; i++)
        {
            cout << "  ";
            for (int j=0; j<rozmiar; j++)
            {
                cout << tabliczka [i][j];
                if(j<2) cout << " | ";
            }
            if(i<2) cout << endl << " ----------- " << endl;
            else cout << endl << endl;
        }

        //Rozgrywka
        cout << "Gracz " << Player << " wybiera miejsce \n>> ";
        cin >> x >> y;
        while(x>3 || x<1 || y>3 || y<1 || tabliczka[x-1][y-1] != ' ')
        {
            cout << "To miejsce jest zajete lub wpisujesz nie poprawna wartosc!\nWybierz inne: \n>>";
            cin >> x >> y;
        }
        tabliczka[x-1][y-1] = Gracz;
        if(Gracz == 'X')
        {
            Gracz = 'O';
            Player = "Kolko";
        }
        else
        {
            Gracz = 'X';
            Player = "Krzyzyk";
        }


        //Sprawdzanie zwycięzy
        //Krzyżyk
       if(
           tabliczka[0][0] == 'X' && tabliczka[0][1] == 'X' && tabliczka[0][2] == 'X' ||
           tabliczka[1][0] == 'X' && tabliczka[1][1] == 'X' && tabliczka[1][2] == 'X' ||
           tabliczka[2][0] == 'X' && tabliczka[2][1] == 'X' && tabliczka[2][2] == 'X' ||
           tabliczka[0][0] == 'X' && tabliczka[1][0] == 'X' && tabliczka[2][0] == 'X' ||
           tabliczka[0][1] == 'X' && tabliczka[1][1] == 'X' && tabliczka[2][1] == 'X' ||
           tabliczka[0][2] == 'X' && tabliczka[1][2] == 'X' && tabliczka[2][2] == 'X' ||
           tabliczka[0][0] == 'X' && tabliczka[1][1] == 'X' && tabliczka[2][2] == 'X' ||
           tabliczka[0][2] == 'X' && tabliczka[1][1] == 'X' && tabliczka[2][0] == 'X'
           ){
            isOver = true;
            Winer = "Krzyzyk";
           }

        //Kółko
        if(
           tabliczka[0][0] == 'O' && tabliczka[0][1] == 'O' && tabliczka[0][2] == 'O' ||
           tabliczka[1][0] == 'O' && tabliczka[1][1] == 'O' && tabliczka[1][2] == 'O' ||
           tabliczka[2][0] == 'O' && tabliczka[2][1] == 'O' && tabliczka[2][2] == 'O' ||
           tabliczka[0][0] == 'O' && tabliczka[1][0] == 'O' && tabliczka[2][0] == 'O' ||
           tabliczka[0][1] == 'O' && tabliczka[1][1] == 'O' && tabliczka[2][1] == 'O' ||
           tabliczka[0][2] == 'O' && tabliczka[1][2] == 'O' && tabliczka[2][2] == 'O' ||
           tabliczka[0][0] == 'O' && tabliczka[1][1] == 'O' && tabliczka[2][2] == 'O' ||
           tabliczka[0][2] == 'O' && tabliczka[1][1] == 'O' && tabliczka[2][0] == 'O'
           ){
            isOver = true;
            Winer = "Kolko";
           }
        if(isOver != true)
        remis = true;

            for(int i=0; i<3; i++)
                for(int j=0; j<3;j++)
                  if(tabliczka[i][j]==' ')
                        remis = false;


        if(remis == true) isOver=true;
    //Czyszczenie ekranu
       system("cls");
    }

    cout << "Kolko i Krzyzyk" << endl << endl;

    for (int i=0; i<rozmiar; i++)
        {
            cout << "  ";
            for (int j=0; j<rozmiar; j++)
            {
                cout << tabliczka [i][j];
                if(j<2) cout << " | ";
            }
            if(i<2) cout << endl << " ----------- " << endl;
            else cout << endl << endl;
        }

    if(remis==true)cout << "Remis!";
    else cout << "Wygrywa: " << Winer << endl;

    // Usuwanie tablicy
    for (int i=0; i<rozmiar; i++)
        delete[] tabliczka[i];
    delete[] tabliczka;

    return 0;

}
