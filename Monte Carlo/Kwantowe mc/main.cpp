#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;


int main()
{
    float R, U, dU, T, dT, Mian, E, Enew;
    float trial, delta, delta2, h;
    const int N=100, x0=-4, xend=4;
    const float dphi=0.1;
    float x[N], phi[N], V[N];
    int a, i;

    srand(time(NULL));

    U=0, T=0, Mian=0;
    h=(xend-x0)/float(N);

    for(i=0; i<=N; i++){
        x[i]=x0+h*(i-1);
        V[i]=0.5*x[i]*x[i];
        R = rand()%100+0;
        R = R/100;
        phi[i]= R;
    }

    for(i=0; i<=N; i++){
        U=U+phi[i]*phi[i]*V[i];
        if (i==0){
            T=T+(0.5*phi[i]*(2*phi[i]-phi[i+1]))/h; //h/h);
        }
        if (i==N){
            T=(T+(0.5*phi[i]*(2*phi[i]-phi[i-1]))/h/h);
        }
        if (i>0 && i<N){
            T=(T+(0.5*phi[i]*(2*phi[i]-phi[i-1]-phi[i+1]))/h/h);
        }
    }

    for(i=0; i<=N; i++){
        Mian=Mian+phi[i]*phi[i];
    }
   /* cout << "U , T = " << U << " , " << T << endl;
    cout << "Mian = " << Mian << endl;
    cout << "E = " << ((U+T)/Mian) << endl;
    */
    for(i=0; i<=0; i++){
        R = rand()%100+0;
        R = R/100;
        a = int(R*N);
        R = rand()%100+0;
        R = R/100;
        trial = (phi[a]+(R-0.5)*dphi);
        delta = trial-phi[i];
        delta2 = trial*trial-phi[i]*phi[i];
        if (i==0){
            dT=(delta2-delta*phi[i+1])/h/h;
        }
        if (i==N){
            dT=(delta2-delta*phi[i-1])/h/h;
        }
        if (i>0 && i<N){
            dT=(delta2-delta*(phi[i+1]+phi[i-1]))/h/h;
        }
        dU=delta2*V[i];
        Enew=(U+T+dU+dT)/(Mian+delta2);
        if (Enew<E){
            phi[i]=trial;
            E=Enew;
            U=U+dU;
            T=T+dT;
            Mian=Mian+delta2;
        }
        cout << "R , a = " << R << " , " << a << endl;
        cout << "trial = " << trial << "=" << phi[a] << "+" << R-0.5 << "*" << dphi << endl;
        cout << "E = " << E << endl;
        cout << "phi[i] = " << phi[i] << endl;
        cout << "trial = " << phi[i] << endl;
    }

    cout << "Eend = " << E << endl;

    return 0;

}
