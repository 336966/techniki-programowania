/*point2d.cpp*/
/*Plik źródłowy klasy Point2d.hpp*/
#include"point2d.hpp"
#include<iostream>
#include<cmath>
#define M_PI 4*atan(1.)

using namespace std;

//konstruktor bez argumentów zwraca punkt (0,0)
//wykorzystano konstruktor z listą inicjalizacyjną
Point2d::Point2d(){
    int x,y;
    cout << "Podaj punkt (x,y) = ";
    cin >> x >> y;
    _R = getR(x,y);
    _Phi = getPhi(x,y);

}

//konstruktor pobierający współrzędne
//wykorzystano konstruktor z listą inicjalizacyjną
Point2d::Point2d(double x, double y)
    : _R(x)
    , _Phi(y){
}

//konstruktor kopiujący
//wykorzystano konstruktor z listą inicjalizacyjną
Point2d::Point2d(const Point2d& other)
    : _R(other._R)
    , _Phi(other._Phi){
}

//przeciążony operator przypisania
Point2d& Point2d::operator= (const Point2d& other){
    //wykorzystano wskaźnik this pokazujący
    //na "ten" obiekt
    this->_R = other._R;
    this->_Phi = other._Phi;
    //operator zwraca "ten" obiekt, aby można było
    //wykonać wielokrotne przypisanie
    return *this;
}

//współrzędna x
double Point2d::getX(){
    return _R;
}

//współrzędna y
double Point2d::getY(){
    return _Phi;
}

//współrzędna r
double Point2d::getR(double x, double y){
    return sqrt(x*x + y*y);
}

//współrzędna phi
double Point2d::getPhi(double x, double y){
    return atan2(y,x);
}

void Point2d::wybor(){
    int a;
    cout << "Wybierz co chcesz zrobic:" << endl;
    cout << "1. Przeksztalcenie jednokladnosci wzgledem (0,0)" << endl;
    cout << "2. Obrot wzgledem poczatku ukladu" << endl;
    cout << "3. Zamknac program" << endl << ">>";
    cin >> a;
    if(a<1 || a>3){
        cout << "Zly wybor!" << endl;
    }
    switch(a){
        case 1: jedno() ;break;
        case 2: kat() ;break;
        case 3: break;
    }
}

void Point2d::jedno(){
    double k;
    cout << "Podaj skale k = ";
    cin >> k;
    _R = (k*_R);
}

void Point2d::kat(){
    float i;
    cout << "Podaj kat [0-2pi] = ";
    cin >> i;
    _Phi = (i+_Phi);
    if (_Phi>=6.28318530718) _Phi = (_Phi-6.28318530718);
}


//przeciążony operator<< dla wypisywania
std::ostream& operator <<(std::ostream& out, Point2d& p){
    return out << "[" << p.getX() << ", " << p.getY() << "]";
}
