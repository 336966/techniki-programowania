/*point2d.hpp*/
/*Plik nagłówkowy klasy Point2d.hpp*/
#ifndef POINT2D_H_INCLUDED
#define POINT2D_H_INCLUDED
#include <iostream>

class Point2d{
    public:
        //konstruktor bez argumentów zwraca punkt (0,0)
        Point2d();
        void wybor();
        void jedno();
        void kat();
        //destruktor nie jest zadeklarowany
        //będzie wykorzystywany domyślny
        //~Point2d();

        //konstruktor pobierający współrzędne
        Point2d(double, double);

        //konstruktor kopiujący
        Point2d(const Point2d& other);

        //przeciążony operator przypisania
        Point2d& operator= (const Point2d& other);

        //gettery i settery
        double getX();
        double getY();
        double getR(double, double );
        double getPhi(double, double );
        void setXY(double, double);
        void setRPhi(double, double);

    private:
        //współrzędne punktu jako pola prywatne
        double _R;
        double _Phi;
};

//deklaracja przeciążonego operatora <<
//na potrzeby wyświetlania współrzędnych punktu
std::ostream& operator<<(std::ostream& out, Point2d& p);

#endif // POINT2D_HPP_INCLUDED
