#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int n;
    cout <<"Podaj liczbe: \n>>";
    cin >> n;
    vector <int> tab[n];

    for(int i=1; i<=n; i++)
        {
        for(int j=1; j<=i; j++)
            {
            if(i%j==0)
                tab[i-1].push_back(j);
            }
        }

    for (int i=0; i<n; i++)
        {
        cout << "Dla liczby: " << i+1 << " sa nastepujace dzielniki: ";
        for (auto j = tab[i].begin(); j != tab[i].end(); j++)
            {
            cout << *j << ' ';
            }
            cout << endl;
        }

    return 0;
}
